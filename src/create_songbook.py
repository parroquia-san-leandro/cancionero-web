import latex_scanner
from os.path import join
from django.template import Engine, Context
from django.conf import settings
import pickle


def generate_songbook(songs, song_numbers, out_file, dj_engine):
    '''Generate an HTML file with a sequence of songs.'''
    # Build the list of songs
    song_list = []
    for n in song_numbers:
        found = None
        for song in songs:
            if song.number == n:
                found = song
                break
        if not found:
            raise Exception("Could not find song with number " + str(n))
        song_list.append(found)
    # Export to html
    context = Context({'songs': song_list})
    with open(out_file, 'w') as f:
        f.write(dj_engine.get_template("songbook.html").render(context))


def create_argparser():
    '''Parse main's arguments.'''
    parser = latex_scanner.create_argparser()
    parser.add_argument("--songs", required=True, nargs='+', type=int, help="A list of song numbers to include.")
    parser.add_argument("--output-file", required=False, nargs=1, default=["misa.html"],
                        help="The file where the result should be placed, relative to --output-dir.")
    return parser


if __name__ == '__main__':
    args = create_argparser().parse_args()
    try:
        with open("songs.dat", "rb") as f:
            songs = pickle.load(f)
    except:
        loader = latex_scanner.SongLoader(args.latex[0])
        if args.other_latex:
            loader.scan_others(args.other_latex[0], args.other_index[0])
        songs = loader.songs
        try:
            with open("songs.dat", "wb") as f:
                pickle.dump(songs, f)
        except:
            pass
    settings.configure(USE_TZ=False, USE_I18N=False)
    e = Engine(dirs=["res/templates/"])
    generate_songbook(songs, args.songs, join(args.output_dir[0], args.output_file[0]), e)
