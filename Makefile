.PHONY=static all clean

ALL_STATIC := $(patsubst res/static/%,public/%,$(wildcard res/static/*))
ALL_LESS := public/main.css public/index.css public/song.css
ALL_TEMPLATES := $(wildcard res/templates/*.html)
PY_SRC := $(wildcard src/**.py)


all: public public/audios public/index.html static

static: $(ALL_LESS) $(ALL_STATIC)

public:
	mkdir public

public/index.html: $(ALL_TEMPLATES) $(PY_SRC)
	python3 src/latex_scanner.py --latex latex/cancionero.tex --audios audios --other-latex latex/canciones/

public/audios: audios public
	rm -f public/audios
	#ln -s ../audios/Canciones public/audios

public/main.css: res/less/main.less res/less/colors.less
	lessc $< $@

public/index.css: res/less/index.less res/less/colors.less
	lessc $< $@

public/song.css: res/less/song.less res/less/colors.less
	lessc $< $@

public/%: res/static/%
	cp -ra $< $@

clean:
	rm -rf public
