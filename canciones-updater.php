<?php

$song_list = "";
foreach ($_POST["song"] as $i)
	if ($i) $song_list .= "$i ";

$cmd  = "python3 /opt/cancionero-web-python/src/create_songbook.py";
$cmd .= " --latex /opt/cancionero-web-python/latex/cancionero.tex";
$cmd .= " --other-latex /opt/cancionero-web-python/latex/canciones/";
$cmd .= " --other-index 400";
$cmd .= " --output-dir /srv/http/canciones-updater/canciones/";
$cmd .= " --output-file result.html";
$cmd .= " --songs $song_list";
echo "<pre>";
passthru("$cmd 2>&1", $return_code);
echo "</pre>";

if ($return_code) {
	http_response_code(500);
	echo "An error occurred!<br/>See the data below<br/>";
	echo "The return code was $return_code<br/>";
	echo "Command: <pre>$cmd</pre>";
	exit();
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<script>
		document.location.replace("https://canciones.sanleandro-obispo.net/misa/update_index.php")
	</script>
</head>
<body>
You can see the result <a href="https://canciones.sanleandro-obispo.net/misa/update_index.php">here</a>.
</body>
</html>
