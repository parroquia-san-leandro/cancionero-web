# Generador de cancioneros web

Este programa crea una página web en base a un cancionero en LaTeX, como por ejemplo el [cancionero del 25 aniversario][0].
Para crearlo, basta con ejecutar `make` (asumiendo que el cancionero está en `latex/cancionero.tex` y los audios en la carpeta `audios/`), y en la carpeta `public/` aparecerá el cancionero.

El resultado puede verse en https://canciones.sanleandro-obispo.net

Dependencias: `python3`, `make`, `less`, `python-django`.

## Características

* **Genera páginas estáticas**, y que por tanto se sirven rápidamente.
* **Enlaza las canciones con sus audios correspondientes**. Para esto, en la carpeta `audios/` se deben colocar. El nombre de cada fichero debe seguir el formato `NNN_YYYY-MM-DD.mp3`, donde `NNN` es el índice numérico de la canción (tres dígitos, rellenar con ceros) y el resto es la fecha de la grabación.
* **Vista adecuada para móvil** (con controles de zoom).
* **Transposición de acordes**.

[0]: https://gitlab.com/parroquia-san-leandro/cancionero25
