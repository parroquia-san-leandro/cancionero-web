/* ===========================================
        FONT SIZE
   =========================================== */
SIZE_STEPS = [30, 50, 67, 80, 90, 100, 110, 120, 133, 150, 170, 200, 240, 300];

currSize = SIZE_STEPS.indexOf(100)

/** Changes the size of the lyrics and chords. */
function size(steps) {
    if (steps === 0) {
      currSize = SIZE_STEPS.indexOf(100);
    }
    currSize += steps;
    document.getElementById('wholeSongDiv').style.fontSize = SIZE_STEPS[currSize] + '%';
}

/* ===========================================
        TRANSPOSE CHORDS
   =========================================== */

ENG_INDEX = {'C': 0, 'C#': 1, 'Db': 1, 'D': 2, 'D#': 3, 'Eb': 3, 'E': 4, 'Fb': 4, 'F': 5, 'E#': 5, 'F#': 6, 'Gb': 6, 'G': 7, 'G#': 8, 'Ab': 8, 'A': 9, 'A#': 10, 'Bb': 10, 'B': 11, 'Cb': 11, 'B#': 0}
LAT_INDEX = {'Do': 0, 'Do#': 1, 'Reb': 1, 'Re': 2, 'Re#': 3, 'Mib': 3, 'Mi': 4, 'Fab': 4, 'Fa': 5, 'Mi#': 5, 'Fa#': 6, 'Solb': 6, 'Sol': 7, 'Sol#': 8, 'Lab': 8, 'La': 9, 'La#': 10, 'Sib': 10, 'Si': 11, 'Dob': 11, 'Si#': 0}
CHORDS_LAT = ['Do', 'Do#', 'Re', 'Re#', 'Mi', 'Fa', 'Fa#', 'Sol', 'Sol#', 'La', 'Sib', 'Si']
CHORDS_ENG = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'Bb', 'B']


/** Changes all chords to a given semitone relative to the original */
function transpose(n) {
    transposeAdd(n - getTranspose())
}

/** Transposes all chords by n steps */
function transposeAdd(n) {
    for (c of document.getElementsByClassName('c')) {
        chord = c.innerHTML
        nbsp = chord.endsWith("&nbsp;")
        if (nbsp) {
            chord = chord.slice(0, -6)
        }
        if (LAT_INDEX[chord] == undefined) {
            throw Error("Unknown chord: " + chord)
        }
        i = LAT_INDEX[chord]
        j = (i + n + 12) % 12
        c.innerHTML = CHORDS_LAT[j] + (nbsp ? "&nbsp;" : "")
    }

    setTransposeSelector(getTranspose() + n)
}

function setTransposeSelector(n) {
    while (n > 6) {
        n -= 12
    }
    while (n < -6) {
        n += 12
    }
    ts = document.getElementById("transposeSelect")
    if (n > 0) {
        ts.value = "+" + n
    } else {
        ts.value = n
    }
}

function getTranspose() {
    return Number.parseInt(document.getElementById("transposeSelect").value)
}

/* ===========================================
        HIDE CHORDS
   =========================================== */

// Init
window.onload = function () {
    var show = localStorage.getItem("show_chords") != "false";
    showChords(show);
    document.getElementById("showChords").checked = show;
};

function showChords(show) {
    if (show || show == "true") {
        for (let e of document.getElementsByClassName("showChords")) { e.style.display = "block"; }
        for (let e of document.getElementsByClassName("showLyrics")) { e.style.display = "none";  }
    } else {
        for (let e of document.getElementsByClassName("showChords")) { e.style.display = "none";  }
        for (let e of document.getElementsByClassName("showLyrics")) { e.style.display = "block"; }
    }
    localStorage.setItem("show_chords", show);
}
